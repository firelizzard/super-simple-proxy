# Super Simple Proxy

Super Simple Proxy (SSP) will proxy HTTP requests to other ports or hosts.

## Listeners

+ `-insecure host:port` enables the insecure (HTTP) listener
  - A value of `-` disables this listener
  - Defaults to `127.0.0.1:80`
+ `-secure host:port` enables the secure (HTTPS) listener
  - A value of `-` disables this listener
  - Defaults to disabled (`-`)
+ `-cert file` specifies the HTTPS server certificate
+ `-key file` specifies the HTTPS server private key

## NetRC Support

+ `-netrc` enables NetRC support.

If NetRC support is enabled, SSP will read `.netrc` (once, at launch), and when
a request is received that corresponds to a NetRC entry, SSP will insert HTTP
Basic credentials into the forwarded request.

## Forwarding

+ `-forward` is a semicolon-separated list of forwarders.

A forwarder is a request matcher and a forwarding target, written as
`<matcher>,<target>`.

### Request Matchers

A request matcher can be a hostname, a hostname and a path, or a URL without a
port:

+ `example.com`
  - Matches requests with `Host: example.com`
+ `example.com/path`
  - Matches requests with `Host: example.com` and a URI equal to `/path` or starting with `/path/`
+ `https://example.com`
  - Matches requests with `Host: example.com` received on the secure (HTTPS) listener

### Forwarding Targets

A forwarding target can be a hostname, a hostname and a port, or a URL:

+ `example.com`
  - Requests received on the insecure (HTTP) listener will be forwarded to `http://example.com:80`
  - Requests received on the secure (HTTPS) listener will be forwarded to `https://example.com:443`
+ `example.com:8080`
  - Requests received on the insecure (HTTP) listener will be forwarded to `http://example.com:8080`
  - Requests received on the secure (HTTPS) listener will be forwarded to `https://example.com:8080`
+ `https://example.com`
  - Requests received on either listener will be forwarded to `https://example.com`