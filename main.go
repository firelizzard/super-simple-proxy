package main

import (
	"flag"
	"io"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"sync"

	"github.com/bgentry/go-netrc/netrc"
)

var insertNetrc = flag.Bool("netrc", false, "insert NetRC credentials")
var insecure = flag.String("insecure", "127.0.0.1:80", "insecure proxy")
var secure = flag.String("secure", "-", "secure proxy")
var cert = flag.String("cert", "", "SSL certificate")
var key = flag.String("key", "", "SSL key")
var forward = flag.String("forward", "", "a semicolon-separated list of host,dest")

var rc *netrc.Netrc

var forwarders = Forwarders{}

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

func parseMatch(s string) (*url.URL, bool) {
	if strings.Contains(s, "://") {
		u, err := url.Parse(s)
		if err != nil {
			log.Printf("Invalid forwarder: failed to parse matcher %q: %v", s, err)
			return nil, false
		}
		if u.Port() != "" {
			log.Printf("Invalid forwarder: matcher must not specify port")
			return nil, false
		}
		return u, true
	}

	i := strings.IndexRune(s, '/')
	if i < 0 {
		return &url.URL{Host: s}, true
	}

	if strings.ContainsRune(s[:i], ':') {
		log.Printf("Invalid forwarder: matcher must not specify port")
		return nil, false
	}

	return &url.URL{Host: s[:i], Path: s[i+1:]}, true
}

func parseTarget(s string) (*url.URL, bool) {
	if strings.Contains(s, "://") {
		u, err := url.Parse(s)
		if err != nil {
			log.Printf("Invalid forwarder: failed to parse forwarder %q: %v", s, err)
			return nil, false
		}
		return u, true
	}

	if !strings.ContainsRune(s, ':') {
		return &url.URL{Host: s}, true
	}

	_, portStr, err := net.SplitHostPort(s)
	if err != nil {
		log.Printf("Invalid forwarder: not a URL or host:port: %q", s)
		return nil, false
	}

	_, err = net.LookupPort("tcp", portStr)
	if err != nil {
		log.Printf("Invalid forwarder: %q is not a recognized port name", portStr)
		return nil, false
	}

	return &url.URL{Host: s}, true
}

func main() {
	flag.Parse()

	ok := true
	for _, s := range strings.Split(*forward, ";") {
		if s == "" {
			continue
		}

		t := strings.Split(s, ",")
		if len(t) != 2 {
			ok = false
			log.Printf("Invalid forwarder: %q", s)
			continue
		}

		match, ok1 := parseMatch(t[0])
		target, ok2 := parseTarget(t[1])
		if !ok1 || !ok2 {
			ok = false
			continue
		}

		forwarders[match.Host] = &Forwarder{match, target, nil}
	}
	if !ok {
		return
	}

	var err error
	if *insertNetrc {
		rc, err = load()
		if err != nil {
			log.Fatal(err)
		}
	}

	var wg sync.WaitGroup

	if *insecure != "" && *insecure != "-" {
		wg.Add(1)

		go func() {
			defer wg.Done()

			log.Printf("Starting proxy server on http://%s (insecure)", *insecure)
			err = http.ListenAndServe(*insecure, &proxy{false})
			if err != nil {
				log.Fatal(err)
			}
		}()
	}

	if *secure != "" && *secure != "-" {
		wg.Add(1)

		go func() {
			defer wg.Done()

			log.Printf("Starting proxy server on https://%s (secure)", *secure)
			err = http.ListenAndServeTLS(*secure, *cert, *key, &proxy{true})
			if err != nil {
				log.Fatal(err)
			}
		}()
	}

	wg.Wait()
}

func load() (*netrc.Netrc, error) {
	if env := os.Getenv("NETRC"); env != "" {
		return netrc.ParseFile(env)
	}

	dir, err := os.UserHomeDir()
	if err != nil {
		return nil, err
	}

	rc, err := netrc.ParseFile(filepath.Join(dir, ".netrc"))
	if err == nil || runtime.GOOS != "windows" || !os.IsNotExist(err) {
		return rc, err
	}

	return netrc.ParseFile(filepath.Join(dir, "_netrc"))
}

type Forwarder struct {
	Match, Target *url.URL

	matchPath []string
}

func (f *Forwarder) MatchPath(p string) bool {
	if f.Match.Path == "" {
		return true
	}

	if f.matchPath == nil {
		f.matchPath = strings.Split(f.Match.Path, "/")
	}

	a, b := f.matchPath, strings.Split(p, "/")
	if len(a) > len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}

	return true
}

type Forwarders map[string]*Forwarder

func (f Forwarders) Find(u *url.URL) (*url.URL, bool) {
	fwd, ok := f[u.Host]
	if !ok {
		return nil, false
	}

	if fwd.Match.Scheme != "" && fwd.Match.Scheme != u.Scheme {
		return nil, false
	}

	if !fwd.MatchPath(u.Path) {
		return nil, false
	}

	return fwd.Target, true
}

type proxy struct{ secure bool }

func (p *proxy) Scheme() string {
	if p.secure {
		return "https"
	} else {
		return "http"
	}
}

func (p *proxy) Forward(req *http.Request) (fwd *http.Request, err error) {
	target, ok := forwarders.Find(&url.URL{Scheme: p.Scheme(), Host: req.Host, Path: req.URL.Path})
	if !ok {
		fwd, err = http.NewRequest(req.Method, req.URL.String(), req.Body)

	} else {
		u := *target
		if u.Scheme == "" {
			u.Scheme = p.Scheme()
		}

		s := u.String()
		if !strings.HasSuffix(s, "/") {
			s += "/"
		}
		s += req.RequestURI

		fwd, err = http.NewRequest(req.Method, s, req.Body)
	}

	for k, v := range req.Header {
		for _, v := range v {
			fwd.Header.Add(k, v)
		}
	}

	return
}

func (p *proxy) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	var m *netrc.Machine
	if *insertNetrc {
		m = rc.FindMachine(req.Host)
	}

	fwd, err := p.Forward(req)
	if err != nil {
		http.Error(w, "server error", http.StatusInternalServerError)
		log.Println(err)
		return
	}

	if m != nil {
		fwd.SetBasicAuth(m.Login, m.Password)
	}

	resp, err := http.DefaultClient.Do(fwd)
	if err != nil {
		http.Error(w, "server error", http.StatusInternalServerError)
		log.Println(err)
		return
	}
	defer resp.Body.Close()

	log.Printf("%s %s", fwd.RemoteAddr, resp.Status)

	for k, v := range resp.Header {
		for _, v := range v {
			w.Header().Add(k, v)
		}
	}

	w.WriteHeader(resp.StatusCode)
	_, err = io.Copy(w, resp.Body)
	if err != nil {
		log.Println(err)
	}
}
